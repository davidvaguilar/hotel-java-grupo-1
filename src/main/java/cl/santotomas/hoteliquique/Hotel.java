/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cl.santotomas.hoteliquique;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author david
 */
public class Hotel {
    private List<Habitacion> habitaciones;
    private List<Reserva> reservas;

    public Hotel() {
        habitaciones = new ArrayList<>();
        reservas = new ArrayList<>();        
    }

    // agregar, editar, eliminar buscar, listar 
    // CRUD

    public List<Habitacion> getHabitaciones() {
        return habitaciones;
    }
    
    
    
    public void agregarHabitacion(Habitacion h){
        habitaciones.add(h);
    }
        
        
    public Reserva hacerReserva(Habitacion habitacion, Cliente cliente, Date fechaEntrada, Date fechaSalida) {
        Reserva reserva = new Reserva(habitacion, cliente, fechaEntrada, fechaSalida);
        reservas.add(reserva);
        return reserva;
    }

    public boolean consultarDisponibilidad(Date fechaEntrada, Date fechaSalida) {
        for (Habitacion habitacion : habitaciones) {
            if (habitacion.isDisponible()) {
                return true;
            }
        }
        return false;
    }
    
    // listar
    public void mostrarHabitaciones(){
        for (Habitacion habitacion : habitaciones){
            System.out.println(habitacion.toString());
        }
    }
    
    
    public void mostrarReservas() {
        for (Reserva reserva : reservas) {
            System.out.println("Cliente: " + reserva.getCliente().getNombre());
            System.out.println("Habitación: " + reserva.getHabitacion().getNumero());
            System.out.println("Fecha de entrada: " + reserva.getFechaEntrada());
            System.out.println("Fecha de salida: " + reserva.getFechaSalida());
            System.out.println("Confirmada: " + reserva.isConfirmada());
            System.out.println();
        }
    }
}
