/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cl.santotomas.hoteliquique;

import java.util.Date;

/**
 *
 * @author david
 */
public class Reserva {
    private Habitacion habitacion;
    private Cliente cliente;
    private Date fechaEntrada;
    private Date fechaSalida;
    private boolean confirmada;

    public Reserva(Habitacion habitacion, Cliente cliente, Date fechaEntrada, Date fechaSalida) {
        this.habitacion = habitacion;
        this.cliente = cliente;
        this.fechaEntrada = fechaEntrada;
        this.fechaSalida = fechaSalida;
        this.confirmada = false;
    }

    public Habitacion getHabitacion() {
        return habitacion;
    }


    public Cliente getCliente() {
        return cliente;
    }

    public Date getFechaEntrada() {
        return fechaEntrada;
    }


    public Date getFechaSalida() {
        return fechaSalida;
    }


    public boolean isConfirmada() {
        return confirmada;
    }

    public void confirmar() {
        confirmada = true;
        habitacion.setDisponible(false);
    }

    @Override
    public String toString() {
        return "Reserva{" + "habitacion=" + habitacion + ", cliente=" + cliente + ", fechaEntrada=" + fechaEntrada + ", fechaSalida=" + fechaSalida + ", confirmada=" + confirmada + '}';
    }
    
    
    

}
